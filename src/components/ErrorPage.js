import React, {Fragment} from 'react';
import { NavLink } from 'react-router-dom';
import {Nav} from 'react-bootstrap';

export default function ErrorPage(){

	return (
		<Fragment>
			<h3>404 Page Not Found</h3>
			<p>The page you are looking for cannot be found</p>
			<Nav.Link as={NavLink} to="/">Home</Nav.Link>
		</Fragment>
	)
}