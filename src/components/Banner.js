import React from 'react';

/*react-bootstrap*/
import{
	Container,
	Row,
	Col,
	Jumbotron,
	Button
} from 'react-bootstrap'

export default function Banner(){

	return(

/*		<div className="container-fluid">
			<div className="row justify-content-center">
				<di className="col-10 col-md-8">
					<div className="jumbotron">
						<h1>Zuitt Coding Bootcamp</h1>
						<p>Opportunities fro everyone, everywhere</p>
						<button className="btn btn-primary">Enroll</button>>
					</div>
				</di>				
			</div>
		</div>*/

		<Container fluid>
			<Row className="mx-1">
				<Col className="px-0">
					<Jumbotron fluid className="px-3">
						<h1>Zuitt Coding Bootcamp</h1>
						<p>Opportunities for everyone, everywhere</p>
						<Button variant="info">Enroll</Button>
					</Jumbotron>
				</Col>
			</Row>
		</Container>
	)
}