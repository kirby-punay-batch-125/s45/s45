import React from 'react'

import {
	Row,
	Col,
	Card,
	Button
} from 'react-bootstrap'

export default function CourseCard(){

	return(

		<Row className="justify-content-center my-5">
			<Col xs={10} md={6}>
				<Card>
				  <Card.Body>
				    <Card.Title>React JS</Card.Title>
				    <Card.Subtitle className="mb-2 text-muted">Description</Card.Subtitle>
				    <Card.Text>
				      Learn React JS
				    </Card.Text>
				 	<Card.Subtitle className="mb-2 text-muted">Price</Card.Subtitle>
				 	<Card.Text>
				 	  PHP 40,000
				 	</Card.Text>
				    <Button variant="info">Enroll</Button>
				  </Card.Body>
				</Card>
			</Col>
		</Row>

	)
}