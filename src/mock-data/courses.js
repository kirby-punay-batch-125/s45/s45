export default [
	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Quasi, consequuntur minima, nihil sunt, totam quidem distinctio ut eveniet perferendis vero reiciendis. Culpa unde rerum iure, corporis optio dolores libero necessitatibus.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Quasi, consequuntur minima, nihil sunt, totam quidem distinctio ut eveniet perferendis vero reiciendis. Culpa unde rerum iure, corporis optio dolores libero necessitatibus.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Quasi, consequuntur minima, nihil sunt, totam quidem distinctio ut eveniet perferendis vero reiciendis. Culpa unde rerum iure, corporis optio dolores libero necessitatibus.",
		price: 55000,
		onOffer: true
	}
]